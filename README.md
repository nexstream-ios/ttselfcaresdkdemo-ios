## TTSelfcareSDK (Tune Talk Selfcare SDK)

A SDK that allow Tune Talk user to perform self registration & other functions. Please contact [Tune Talk](https://tunetalk.com/my/en) if you have any enquiry.

## Requirements

- iOS 9.0+
- Xcode 8.0+
- Swift 3.0+

## Installation

### CocoaPods

[CocoaPods](https://cocoapods.org) is a dependency manager for Cocoa projects. You can install it with the following command:

```bash
$ gem install cocoapods
```

> CocoaPods 1.5+ is required to build TTSelfcareSDK

To integrate TTSelfcareSDK into your Xcode project using CocoaPods, specify it in your `Podfile`:

```ruby
use_frameworks!

target '<Your Target Name>' do
pod 'TTDealerSDK', :git => 'https://gitlab.com/ashtontam92/TTSelfcareSDKFramework-iOS.git'

end
```

Install Git LFS so that it can install large library, skip this step if it's installed:

```bash
To use Homebrew, run brew install git-lfs.
To use MacPorts, run port install git-lfs.
```

Then, run the following command:

```bash
$ pod install
```

## Version 3.0.0 (Set Up For Microblink SDK)
1. Drag or Add the Microblink.framework (5.4.1)  into your project and in the Frameworks,Libraries and Embedded Content row set Microblink.framework's embed to embed with Sign In.

2. Drag or Add the Microblink.bundle into your project. 

## Start Up
#### [Init](#Init)
1. First, you are **COMPULSARY** to call for our init function, and use the callback provided to check whether the SDK had been initiated successful or not.

2. For integration, refer to Advanced Integrations.

```
SelfcareSDK(apiKey: "apikey123", isProduction: false, completion: { (isInit, errorDescription) in
print("isInit: \(isInit), errorDescription: \(errorDescription ?? "nil")")
})
```


## Advanced Integrations (Only can be used after init successfully)

#### [Step One Registration](#StepOneRegistration)
<!--This method is used for step 1 sim card registration. <br/>-->

```
SelfcareSDK.shared.regStepOne(target: self) { (resultCode, errorDescription, registrationId) in
print("resultCode: \(resultCode), errorDescription: \(errorDescription ?? "nil"), registrationId: \(registrationId ?? "nil")")

if let regID = registrationId{
}

self.navigationController?.popToRootViewController(animated: true)
}
```

#### [Step Two Registration](#StepTwoRegistration)
<!--This method is used for step 2 sim card registration. <br/>-->

```
SelfcareSDK.shared.regStepTwo(target: self, ICCID: "YourSIMCardICCID", registrationID: "YourRegistrationID", isSelectNumber: true, completion: { (resultCode, errorDescription, regDetail) in
print("resultCode: \(resultCode), errorDescription: \(errorDescription ?? "nil"), firstName: \(regDetail?.firstName ?? "nil"), lastName:\(regDetail?.lastName ?? "nil"), idType:\(regDetail?.idType ?? "nil"), idNumber:\(regDetail?.idNumber ?? "nil"), dob:\(regDetail?.dob ?? "nil"), address:\(regDetail?.address ?? "nil"), city:\(regDetail?.city ?? "nil"), state:\(regDetail?.state ?? "nil"), postCode:\(regDetail?.postCode ?? "nil"), country:\(regDetail?.country ?? "nil"), msisdn:\(regDetail?.msisdn ?? "nil")")

self.navigationController?.popToRootViewController(animated: true)
})
```

## Request and Result

### <a name="Init"></a>Init
**Request:**

|Argument       |      Type           | Required/Optional| Description                                                        |
|-----------    |----------           |------------------|---                                                                 |
|apiKey       |String             | Required         | Your api key from Tune Talk                                                  |
|isProduction       |Bool             | Required         | The connection type to our SDK server, either production or staging                                                  |

**Result:**

|Param              |      Type     | Description                         |
|-----------        |----------     |---                                  |
|isInit         |Bool            | The status of init whether success or failure |
|errorDescription   |String?         | If failure, this string contains the reason of the error |

### <a name="StepOneRegistration"></a>Step One Registration
**Request:**

|Argument       |      Type           | Required/Optional| Description                                                        |
|-----------    |----------           |------------------|---                                                                 |
|target       |UIViewController             | Required         | Your current view controller                                                  |

**Result:**

|Param              |      Type     | Description                         |
|-----------        |----------     |---                                  |
|resultCode         |Int            | Verification status. Possible values: <br/> 0: Successful <br/> 1: User Cancelled (Not available) <br/> 2: Verification Failed <br/> 3: User reach max 5 SIM limit <br/>  |
|errorDescription   |String?         | If failure, this string contains the reason of the error|
|registrationId   |String?         | If successful, this string will return the registration ID that required for registration step 2|

### <a name="StepTwoRegistration"></a>Step Two Registration
**Request:**

|Argument       |      Type           | Required/Optional| Description                                                        |
|-----------    |----------           |------------------|---                                                                 |
|target       |UIViewController             | Required         | Your current view controller                                                  |
|ICCID                   |String               | Optional         | SIM ICCID to be registered, prompt for SIM card scanner if input nil value                          |
|registrationID              |String               | Optional         | The registration ID that received from Step One Registration for successful verification                           |
|isSelectNumber              |Bool              | Required         | A flag that decide user should prompt to select mobile number controller. if false, random mobile number will assign to the SIM card                           |

**Result:**

|Param              |      Type     | Description                         |
|-----------        |----------     |---                                  |
|resultCode         |Int            | Verification status. Possible values: <br/> 0: Successful <br/> 1: User Cancelled (Not available) <br/> 2: Empty Registration ID <br/> 3: Invalid Registration ID <br/> 4: Registration Failure <br/> 5: Invalid SIM ICCID <br/> |
|errorDescription   |String?         | If failure, this string contains the reason of the error|
|regDetail   |RegistrationDetail?         | The registration detail of the user that being registered|

**RegistrationDetail:**

|Param              |      Type     | Description                         |
|-----------        |----------     |---                                  |
|firstName         |String            | The first name of the registrant|
|lastName         |String            | The last name of the registrant|
|idType         |String            | The type of ID used. NRIC or passport|
|idNumber         |String            | The ID number. NRIC or passport|
|dob         |String            | The date of birth of the registrant|
|address         |String            | The address of the registrant|
|city         |String            | The city of the registrant|
|state         |String            | The state of the registrant|
|postCode         |String            | The postcode of the registrant|
|country         |String            | The country of the registrant|
|msisdn         |String            | The mobile number that registered under the SIM card|

