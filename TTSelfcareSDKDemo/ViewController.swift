//
//  ViewController.swift
//  TTSelfcareSDKDemo
//
//  Created by AshtonTam on 26/10/2018.
//  Copyright © 2018 Tunetalk. All rights reserved.
//

import UIKit
//import MGLivenessDetection
import TTSelfcareSDK

class ViewController: UIViewController {
    
    
    @IBOutlet weak var registrationIDTextField: UITextField!
    @IBOutlet weak var ICCIDTextfield: UITextField!
    @IBOutlet weak var ESIMICCIDTF: UITextField!
    @IBOutlet weak var shouldSelectNumber: UISwitch!
    @IBOutlet weak var simRepESIMICCIDTF: UITextField!
    @IBOutlet weak var responseLbl: UILabel!
    let apiKey = "568e03e3-2cb4-44c8-b510-7785f40fd907";
    let isProduction = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Tune Talk SDK Demo"
        
        let _ = SelfcareSDK(apiKey: apiKey, isProduction: isProduction, completion: { (isInit, errorDescription) in
            print("isInit: \(isInit), errorDescription: \(errorDescription ?? "nil")")

            self.responseLbl.text = "API: sdkInit \nisInit: \(isInit) \nerrorDescription: \(errorDescription ?? "nil")"
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    @IBAction func ScanPressed(_ sender: Any) {
        SelfcareSDK.shared.regStepOne(target: self, apiKey: apiKey) { (resultCode, errorDescription, registrationId, documentImageBase64Str) in
            print("resultCode: \(resultCode), errorDescription: \(errorDescription ?? "nil"), registrationId: \(registrationId ?? "nil")")
            
            self.responseLbl.text = "API: regStepOne \nresultCode: \(resultCode) \nerrorDescription: \(errorDescription ?? "nil") \nregistrationId: \(registrationId ?? "nil")"
            
            if let regID = registrationId{
                self.registrationIDTextField.text = regID
            }
            
            if let str = documentImageBase64Str{
                let decodedData = Data(base64Encoded: str, options: .ignoreUnknownCharacters)
                if let data:Data = decodedData {
                    if let documentImage = UIImage(data: data){
                        //Get Document Image here
                    }
                }
            }
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        //        if (MGLiveManager.getLicense()) {
        //            let liveManager = MGLiveManager()
        //
        //            liveManager.actionCount = 2
        //            liveManager.actionTimeOut = 10
        //            liveManager.randomAction = true
        //
        //            liveManager.startFaceDecetionViewController(self, finish: { (data, target) in
        //                self.dismiss(animated: false, completion: nil)
        //            }) { (type, target) in
        //                self.dismiss(animated: false, completion: nil)
        //            }
        //        }
    }
    
    @IBAction func chooseNumberPressed(_ sender: Any) {
        var esimIccid: String?
        if !ESIMICCIDTF.isEmpty {
            esimIccid = ESIMICCIDTF.text
        }
        
        SelfcareSDK.shared.regStepTwo(target: self, apiKey: apiKey, ICCID: ICCIDTextfield.text, ESIMICCID: esimIccid, registrationID: self.registrationIDTextField.text, isSelectNumber: shouldSelectNumber.isOn, completion: { (resultCode, errorDescription, regDetail) in
            print("resultCode: \(resultCode), errorDescription: \(errorDescription ?? "nil"), firstName: \(regDetail?.firstName ?? "nil"), lastName:\(regDetail?.lastName ?? "nil"), idType:\(regDetail?.idType ?? "nil"), idNumber:\(regDetail?.idNumber ?? "nil"), dob:\(regDetail?.dob ?? "nil"), address:\(regDetail?.address ?? "nil"), city:\(regDetail?.city ?? "nil"), state:\(regDetail?.state ?? "nil"), postCode:\(regDetail?.postCode ?? "nil"), country:\(regDetail?.country ?? "nil"), msisdn:\(regDetail?.msisdn ?? "nil")")
            
            self.responseLbl.text = "API: regStepTwo \nresultCode: \(resultCode) \nerrorDescription: \(errorDescription ?? "nil") \nfirstName: \(regDetail?.firstName ?? "nil"), lastName:\(regDetail?.lastName ?? "nil"), idType:\(regDetail?.idType ?? "nil"), idNumber:\(regDetail?.idNumber ?? "nil"), dob:\(regDetail?.dob ?? "nil"), address:\(regDetail?.address ?? "nil"), city:\(regDetail?.city ?? "nil"), state:\(regDetail?.state ?? "nil"), postCode:\(regDetail?.postCode ?? "nil"), country:\(regDetail?.country ?? "nil"), msisdn:\(regDetail?.msisdn ?? "nil"))"
            
            self.navigationController?.popToRootViewController(animated: true)
        })
    }
    
    @IBAction func simReplacementPressed(){
        var esimIccid: String?
        if !simRepESIMICCIDTF.isEmpty {
            esimIccid = simRepESIMICCIDTF.text
        }
        
        SelfcareSDK.shared.simReplacement(target: self, apiKey: apiKey, ESIMICCID: esimIccid, completion: {
            (resultCode, errorDescription, simReplacementDetail) in
            self.responseLbl.text = "API: simReplacement \nresultCode: \(resultCode) \nerrorDescription: \(errorDescription ?? "nil") \nfirstName: \(simReplacementDetail?.firstName ?? "nil"), lastName:\(simReplacementDetail?.lastName ?? "nil"), idType:\(simReplacementDetail?.idType ?? "nil"), idNumber:\(simReplacementDetail?.idNumber ?? "nil"), dob:\(simReplacementDetail?.dob ?? "nil"), address:\(simReplacementDetail?.address ?? "nil"), city:\(simReplacementDetail?.city ?? "nil"), state:\(simReplacementDetail?.state ?? "nil"), postCode:\(simReplacementDetail?.postCode ?? "nil"), country:\(simReplacementDetail?.country ?? "nil"), msisdn:\(simReplacementDetail?.msisdn ?? "nil"))"
            
            self.navigationController?.popToRootViewController(animated: true)
        })
    }
}
